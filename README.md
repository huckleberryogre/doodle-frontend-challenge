## Doodle frontend challenge

### Description
Simple chat application.

Tech stack: Svelte, TypeScript, Vite

Tested with Jest (unit) and Playwright (e2e)

### Installation

* `yarn install`
* create `.env` file and paste your api token inside (see `.env.sample` file):
```
VITE_API_TOKEN=%YOUR_API_TOKEN%
```

### Run development
* `yarn run dev`
* http://localhost:3000

### Build production
`yarn run duild`

### Unit tests
`yarn run test`

### e2e tests
* `npx playwright install` (once)
* run app on http://localhost:3000
* `yarn run test:e2e`

### Usage
At own risk 🙂

### Roadmap
next iteration: telegram 😎
