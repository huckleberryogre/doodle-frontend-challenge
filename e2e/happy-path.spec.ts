import {test} from '@playwright/test';

/**
 * Preset:
 */
const randomNumber = Math.floor(Math.random() * 10000);
const nickname = `Autotest #${randomNumber}`;
const greeting = `Moin! #${randomNumber}`;

test('happy path test', async ({page}) => {
    page.on('dialog', async dialog => {
        await dialog.accept(nickname);
    });

    await page.goto('http://localhost:3000/');

    await page.type('[data-testid="input"]', greeting, {delay: 100});

    await page.click('[data-testid="button"]', {delay: 1000});

    await page.waitForSelector(`text="${nickname}"`);

    await page.waitForSelector(`text="${greeting}"`);
});
