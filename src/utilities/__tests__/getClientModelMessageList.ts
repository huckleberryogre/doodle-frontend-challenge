import {getHumanReadableDate} from "../getHumanReadableDate";

/**
 * ! Test cases are valid for the time zone GMT+2
 */

test('should correctly display uni-digit hours and minutes', () => {
    const result = getHumanReadableDate(1628384461000);

    expect(result).toBe("8 Aug 2021 03:01");
});

test('should correctly display edgy case', () => {
    const result = getHumanReadableDate(949359600000);

    expect(result).toBe("1 Feb 2000 00:00");
});
