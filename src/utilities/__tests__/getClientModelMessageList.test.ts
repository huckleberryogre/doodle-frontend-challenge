import {getClientModelMessageList} from "../getClientModelMessageList";

test('should return an empty array', () => {
    const result = getClientModelMessageList([]);

    expect(result).toEqual([]);
});

test('should correctly trim model', () => {
    const result = getClientModelMessageList([{message: '1', author: 'Tom', timestamp: 123, token: 'test', _id: 'uuid'}]);

    expect(result).toEqual([{message: '1', author: 'Tom', timestamp: 123}]);
});
