const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

/**
 * Parses given unix timestamp. Returns a human readable date in a format "DD mm YYYY HH:MM"
 */
export const getHumanReadableDate = (date: number): string => {
    try {
        const dateInstance = new Date(+date)
        const year = dateInstance.getFullYear();
        const month = dateInstance.getMonth();
        const day = dateInstance.getDate();
        const hour = dateInstance.getHours();
        const min = dateInstance.getMinutes();

        return `${day} ${months[month]} ${year} ${hour < 10 ? '0' + hour : hour}:${min < 10 ? '0' + min : min}`;
    } catch (error) {
        console.error(`failed parsing given date: ${date}`, error);
        return '';
    }
};
