import type {IMessage} from "../models";

/**
 * Server -> client model mapper.
 *
 * Returns client model message list.
 */
export const getClientModelMessageList = (messageList: IMessage[]): IMessage[] => messageList.map(message => {
    // removes unused client fields
    delete message._id;
    delete message.token;

    return message;
});