import '@testing-library/jest-dom/extend-expect'

import {render, fireEvent} from '@testing-library/svelte';

import Button from '../Button.svelte';
import {EButtonColor} from "../enums";

test('button should be in the document', () => {
    const {getByTestId} = render(Button);

    expect(getByTestId('button')).toBeInTheDocument();
});

test('button should have "orange" class', () => {
    const {getByTestId} = render(Button, {color: EButtonColor.ORANGE});

    expect(getByTestId('button')).toHaveClass('orange');
});

test('button should call onClick when hit enter key', async () => {
    const onClick = jest.fn();

    const {getByTestId} = render(Button, {onClick});

    const button = getByTestId('button');


    await fireEvent(button, new KeyboardEvent('keydown', {'key': 'Enter'}));

    expect(onClick).toHaveBeenCalled();
})