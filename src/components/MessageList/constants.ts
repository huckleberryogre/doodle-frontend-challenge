/**
 * User OS is windows.
 */
export const isWindows = window.navigator.userAgent.includes("Windows");

/**
 * Style that compensate shift which is added by a browser scrollbar on windows OS (from the right side).
 */
export const scrollBarPaddingCompensationStyle = `padding-right: calc(10% - 17px)`;
