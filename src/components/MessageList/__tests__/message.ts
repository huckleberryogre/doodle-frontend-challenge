import '@testing-library/jest-dom/extend-expect'

import {render} from '@testing-library/svelte';

import Message from '../Message.svelte';
import {getHumanReadableDate} from "../../../utilities";

test('message should be in the document and with given attributes', () => {
    const {getByTestId} = render(Message, {
        author: 'Tom',
        message: 'hello',
        timestamp: Date.now(),
        own: true,
    });
    const message = getByTestId('message');

    expect(message).toBeInTheDocument();
    expect(message).toHaveClass('own');
    expect(document.querySelector('[data-testid="message"] .author')).toHaveTextContent('Tom');
    expect(document.querySelector('[data-testid="message"] .message-text')).toHaveTextContent('hello');
    expect(document.querySelector('[data-testid="message"] .date')).toHaveTextContent(`${getHumanReadableDate(Date.now())}`);
});
