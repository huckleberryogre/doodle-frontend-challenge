import '@testing-library/jest-dom/extend-expect'

import {render, fireEvent} from '@testing-library/svelte';

import Input from '../Input.svelte';

test('input should be in the document and with given attributes', () => {
    const {getByTestId} = render(Input, {required: true, value: 'test'});
    const input = getByTestId('input');

    expect(input).toBeInTheDocument();
    expect(input).toHaveValue('test');
    expect(input).toHaveAttribute('required');
});

test('should support declared events', async () => {
    const onInput = jest.fn();
    const onKeyDown = jest.fn();

    const {getByTestId} = render(Input, {onInput, onKeyDown});

    const input = getByTestId('input');

    await fireEvent.keyDown(input);
    await fireEvent.input(input);

    expect(onInput).toHaveBeenCalled();
    expect(onKeyDown).toHaveBeenCalled();
})