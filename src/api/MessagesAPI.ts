import type {IMessage} from "../models";

const {VITE_API_TOKEN} = import.meta.env;

export const MessagesAPI = {
    fetchAll: async (): Promise<IMessage[]> => {
        return await fetch(`https://chatty.kubernetes.doodle-test.com/api/chatty/v1.0/?token=${VITE_API_TOKEN}`)
            .then(response => response.json());
    },
    send: async (message: string, author: string): Promise<IMessage> => {
        return await fetch(`https://chatty.kubernetes.doodle-test.com/api/chatty/v1.0`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                token: `${VITE_API_TOKEN}`
            },
            body: JSON.stringify({
                message,
                author,
            })
        }).then(response => response.json());
    }
};
